FROM php:7.4-apache

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd

RUN apt-get update && apt-get install -y \
        zlib1g-dev libicu-dev g++ \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl

RUN docker-php-ext-configure pdo_mysql \
    && docker-php-ext-install pdo_mysql

RUN apt-get update && apt-get install -y \
    default-mysql-client

RUN apt-get update -y \
    && apt-get install -y \
        libxml2-dev \
    && apt-get clean -y \
    && docker-php-ext-install soap	

RUN docker-php-ext-install sockets

RUN apt-get install -y \
        libzip-dev \
        zip \
  && docker-php-ext-install zip

RUN docker-php-ext-install bcmath

RUN apt-get install -y \
        libxslt-dev \
    && docker-php-ext-install xsl

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y ssl-cert \
    && rm -r /var/lib/apt/lists/*

RUN a2enmod ssl \
    && a2ensite default-ssl

RUN a2enmod rewrite

RUN apt-get update && apt-get install -y \
        git \
        zsh \
        fonts-powerline

RUN pecl install xdebug-3.1.5 \
    && docker-php-ext-enable xdebug

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN usermod -d /home/www-data www-data \
    && usermod -u 1000 www-data \
    && groupmod -g 1000 www-data
